(/**
 * @param {Document} d
 */
    function (d) {

    var
        getRegExp = function (className) {
            return new RegExp('(\\s|^)' + className + '(\\s|$)');
        },
        hasClass = function (el, className) {
            if (el.classList)
                return el.classList.contains(className);
            else
                return !!el.className.match(getRegExp(className))
        },
        addClass = function (el, className) {
            if (el.classList)
                el.classList.add(className);
            else if (!hasClass(el, className))
                el.className += " " + className
        },

        removeClass = function (el, className) {
            if (el.classList)
                el.classList.remove(className);
            else if (hasClass(el, className)) {
                el.className = el.className.replace(getRegExp(className), ' ');
            }
        },

        updateLabelChecks = function () {
            var inputs = d.querySelectorAll('[type=checkbox],[type=radio]'), i, j, input, labels, label;
            for (i in inputs) {
                if (inputs.hasOwnProperty(i)) {
                    input = inputs[i];
                    if (input.id && (labels = d.querySelectorAll('label[for="' + input.id + '"]'))) {
                        for (j in labels) {
                            if (labels.hasOwnProperty(j)) {
                                label = labels[j];
                                if (input.checked)
                                    addClass(label, 'checked');
                                else
                                    removeClass(label, 'checked');
                            }
                        }
                    }
                }
            }
        };
    d.addEventListener('change', updateLabelChecks);
    d.addEventListener('DOMContentLoaded', updateLabelChecks);
    d.addEventListener('load', updateLabelChecks);
})(document);