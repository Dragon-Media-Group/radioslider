<html lang="nl">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="assets/styles.css">
</head>
<body>

<div class="slides">

    <?php

    $r = range(1, $last = rand(10, 20));

    foreach ($r as $i):
        ?>
        <input type="radio" name="slide" id="slide-<?= $i ?>" class="slide-radio"<?= $i < 2 ? ' checked' : '' ?>>
        <div class="slide" data-i="<?= $i ?>">
            <div class="slide-container">
                <div class="slide-image">
                    <img src="//placehold.it/160x90">
                </div>
                <div class="slide-title">
                    Slide <?= $i ?>
                </div>
                <div class="slide-text">
                    Een text
                </div>

                <div class="slide-arrows">
                    <?php
                    if ($i > 1):
                        ?>
                        <label class="left" for="slide-<?= $i - 1 ?>">&lt;</label>
                        <?php
                    endif;
                    ?>
                    <?php
                    if ($i!= $last):
                        ?>
                        <label class="right" for="slide-<?= $i + 1 ?>">&gt;</label>
                        <?php
                    endif;
                    ?>
                </div>
            </div>
        </div>
        <?php
    endforeach;
    ?>
</div>

<div class="labels">
    <?php
    foreach ($r as $i):
        ?>
        <label for="slide-<?= $i ?>">
            Slide <?= $i ?>
        </label>
        <?php
    endforeach;
    ?>
</div>
<script src="assets/script.js"></script>
</body>
</html>